import uiRouter from 'angular-ui-router';

var module = angular.module('app.constant', [
    uiRouter
    //import modules
]);

module.constant("CONSTANTS", {
    APPLICATION_NAME: "VBRS",
    SERVER_URL: "http://localhost:3600",
    FILE_UPLOAD_URL: "http://localhost:4100",
    SITE_URL: "http://localhost:3800/"
})
