import template from './dashboard.html';
import controller from './dashboard.controller';
//import './login.scss'

var dashboardModule = angular.module('app.dashboard', []);

// loading components, services, directives, specific to this module.
dashboardModule.component('dashboard', {
    template: template,
    controller: controller,
    controllerAs:'dashboard'
}).controller('dashboardCtrl', controller);

// export this module
export default dashboardModule;