export default class Dashboard {
    constructor($scope, $state, storage) {
        "ngInject";
        /*  To change the title of page*/
        $scope.$emit("titleofpage", { title: "Dashboard", hideBack: true });
        this.$onInit = () => {
            let isLoggedIn = storage.getItem("isUserLoggedIn") ? storage.getItem("isUserLoggedIn") : false
            if (!isLoggedIn) {
                $state.go("login", {}, {
                    reload: true
                })
            }
        }


    }
}
