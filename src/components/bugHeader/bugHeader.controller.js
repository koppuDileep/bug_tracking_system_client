export default class bugHeader {
    constructor($scope, $state, $rootScope, apiService, APIURLCONST, storage, CONSTANTS) {
        'ngInject';
        /*  To change the title of page*/
        $rootScope.$on('titleofpage', function (event, data) {
			$rootScope.hideBack = data.hideBack
            $scope.title = data.title
            $scope.currentUser = storage.getItem("username") ? storage.getItem("username") : ""
        });

        $scope.appName = CONSTANTS.APPLICATION_NAME;
        $rootScope.hideBack = true;
        /* To navigate login page  */
        $scope.logout = function () {
            console.log("Logout")
            apiService.callAPI(APIURLCONST.API_LOGOUT, "GET", {}, function (resultObj) {
                if (resultObj.isSuccess) {
                    toastr.success("You have successfully logged out")
                    storage.clear();
                    $state.go("login");
                } else {
                    console.log("error")
                }
            })
        }

        $scope.dashboard = () => {
            $state.go("dashboard")
        }

        $scope.goBack = () => {
            if ($scope.currentUser != null)
                window.history.back()
            else
                window.location.reload();
        }

        $scope.profile = () => {
            $state.go("myProfile");
        }
    }

}