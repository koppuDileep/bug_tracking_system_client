import template from './bugHeader.html';
import controller from './bugHeader.controller';
//import './login.scss'

var bugHeaderModule = angular.module('app.bugHeader', []);

bugHeaderModule.run(function ($state, $rootScope) {
    $rootScope.$state = $state;
})

// loading components, services, directives, specific to this module.
bugHeaderModule.component('bugHeader', {
    template: template,
    controller: controller,
    controllerAs: 'bugHeader'
}).controller('bugHeaderCtrl', controller);

// export this module
export default bugHeaderModule;