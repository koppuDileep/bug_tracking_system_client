
export default class SideNav {
	constructor($state, $scope, $rootScope, DATACONST, storage) {
		"ngInject";

		this.$onInit = () => {
			let isLoggedIn = storage.getItem("isUserLoggedIn") ? storage.getItem("isUserLoggedIn") : false
			if (!isLoggedIn) {
				$state.go("tora", {}, {
					reload: true
				})
			} else {
				$rootScope.updateSideNav()
			}
		}

		$scope.isExapand = false;
		$scope.isActive = true;

		$rootScope.updateSideNav = () => {
			$scope.features = DATACONST.SIDENAV_FEATURES
		}

		$scope.gotoFeature = data => {
			console.log(data)
			$rootScope.hideBack = false
			$scope.isActive = false
			_.each($scope.features, f => {
				if(f.f_id == data.f_id){
					f.active = true
				} else {
					f.active = false
				}
			})
			$state.go(
				data.state,
				{
					reload: true
				}
			);
			angular.element(document.querySelector("#tora-sidenav")).removeClass("expanded");
		};

		$scope.goHome = () => {
			_.each($scope.features, f => {
				f.active = false
			})
			$scope.isActive = true;
			$state.go('dashboard')			
			angular.element(document.querySelector("#tora-sidenav")).removeClass("expanded");
		}

        $scope.logout = () => {
            storage.clearAll();
            $state.go("tora")
        }

		$rootScope.toggleLeft = () => {
			$scope.isExapand = !$scope.isExapand;
			if($scope.isExapand){
				angular.element(document.querySelector("#tora-sidenav")).addClass("expanded");
			} else {
				angular.element(document.querySelector("#tora-sidenav")).removeClass("expanded");
			}
		};
	}
}
