import template from './sidenav.html';
import controller from './sidenav.controller';


var sidenavModule = angular.module('app.sidenav', []);

// loading components, services, directives, specific to this module.
sidenavModule.component('sidenav', {
    template: template,
    controller: controller,
    controllerAs:'sidenavCtrl'
}).controller('sidenavCtrl', controller);

// export this module
export default sidenavModule;