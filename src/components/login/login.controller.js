export default class Login {
	constructor($scope, $rootScope, $state, apiService, APIURLCONST, storage) {
		'ngInject';

		let isUserLoggedIn = storage.getItem("isLoggedIn");
		if (isUserLoggedIn) {
			$state.go("dashboard");
		}

		$scope.login = () => {
			apiService.callAPI(APIURLCONST.API_LOGIN, "POST", {
				email: $scope.login.email,
				password: $scope.login.password,
				type: "web"
			}, function (response) {
				if (response.isSuccess) {
					storage.setItem("loginDetails", response.data);
					storage.setItem("role", response.data.user_details.role_id);
					storage.setItem("token", response.data.token);
					storage.setItem("user_id", response.data.user_details._id);
					storage.setItem("username", response.data.user_details.username)
					storage.setItem("is_register", response.data.user_details.is_register)
					storage.setItem("email", response.data.user_details.email)
					storage.setItem("isUserLoggedIn", true)
					toastr.success("Login Successfully");
					$rootScope.updateSideNav()
					$state.go("dashboard")

				}
			})
		}

	}
}