import template from './login.html';
import controller from './login.controller';
//import './login.scss'

var loginModule = angular.module('app.login', []);

// loading components, services, directives, specific to this module.
loginModule.component('login', {
    template: template,
    controller: controller,
    controllerAs:'loginCtrl'
}).controller('loginCtrl', controller);

// export this module
export default loginModule;