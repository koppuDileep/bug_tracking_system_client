export default [
	{
		name: 'bugHeader',
		url: '/bugHeader',
		component: 'bugHeader'
	},{
		name: 'login',
		url: '/login',
		component: 'login'
	},
	{
		name: 'sidenav',
		url: '/sidenav',
		component: 'sidenav'
	}
];
