import routerHelperService from './router-helper/router-helper.service';
import toastr from 'toastr'
import moment from 'moment'
const coreModule = angular.module('app.core', [
    'ui.router',
    'oc.lazyLoad',

]);

// inject services, config, filters and re-usable code
// which can be shared via all modules
coreModule.config(routerHelperService);

/******************* SERVICES - START **********************/
//Server API call
coreModule
    .service("apiService", function (CONSTANTS, DATACONST, storage) {
        /*
         * endpoint - Server endpoint 
         * method - GET/POST/PUT/DELETE
         * args - arguments to be passed to the server
         * callback - callback function after getting the response from the server
         */
        this.callAPI = (endpoint, method, args, callback) => {
            try {
                this.NODE_URL = CONSTANTS.SERVER_URL
                var url = this.NODE_URL + endpoint;
                console.log(url)
                console.log(args)
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        try {
                            let result = JSON.parse(xhttp.responseText);
                            console.log(result)
                            if (result.status.code == 200 || result.status.code == 241) {
                                console.log(result.data)
                                callback({
                                    isSuccess: true,
                                    data: result.data
                                })
                            } else {
                                if (result.status.code == 422) {
                                    callback({
                                        isSuccess: false,
                                        isValidationError: true,
                                        error: result.error
                                    })
                                } else {
                                    console.log("safsdhjkfhksjdhfjlkdshfkj")
                                    callback({
                                        isSuccess: false,
                                        error: DATACONST.ERRORS[result.error.message] ? DATACONST.ERRORS[result.error.message] : DATACONST.ERRORS["error"]
                                    })
                                }
                            }
                        } catch (errors) {
                            console.log(errors)
                            callback({
                                isSuccess: false,
                                error: DATACONST.ERRORS.error
                            });
                        }
                    } else {
                        if (this.readyState == 4) {
                            console.log(JSON.parse(xhttp.responseText))
                            callback({
                                isSuccess: false,
                                error: DATACONST.ERRORS.error
                            });
                        }
                    }
                };
                xhttp.open(method, url, true);
                xhttp.setRequestHeader("Content-type", "application/json");
                xhttp.setRequestHeader("x-auth-token", storage.getItem("token"));
                xhttp.setRequestHeader("x-auth-userid", storage.getItem("user_id"));
                xhttp.onerror = function (e) {
                    callback({
                        isSuccess: false,
                        error: DATACONST.ERRORS.error
                    });
                };
                if (args != undefined) {
                    console.log(JSON.stringify(args))
                    xhttp.send(JSON.stringify(args));
                } else {
                    xhttp.send();
                }
            } catch (ex) {
                console.log(ex);
            }
        }
    })
    .service("fileServices", function (Upload, CONSTANTS, APIURLCONST, $http, storage) {
        this.viewFile = (filePath) => {
            return CONSTANTS.FILE_UPLOAD_URL + filePath
        }

        var fileFormats = {
            image: "jpeg, jpg or png",
            audio: "mp3, ogg or wav",
            pdf: "pdf",
            video: "mp4"
        }

        var validateFileFormat = (type, name) => {
            var isSupportedFormat = false;
            if (type === "image") {
                let extValue = name.split('.').pop();
                if (extValue === 'jpeg' || extValue === 'jpg' || extValue === 'png') {
                    isSupportedFormat = true
                }
            } else if (type === "audio") {
                let extValue = name.split('.').pop();
                if (extValue === 'mp3' || extValue === 'ogg' || extValue === 'wav') {
                    isSupportedFormat = true;
                }
            } else if (type === "pdf") {
                let extValue = name.split('.').pop();
                if (extValue === 'pdf') {
                    isSupportedFormat = true;
                }
            } else if (type === "video") {
                let extValue = name.split('.').pop();
                if (extValue === 'mp4') {
                    isSupportedFormat = true;
                }
            }
            else if (type === "pdf") {
                let extValue = name.split('.').pop();
                if (extValue === 'pdf') {
                    isSupportedFormat = true;
                }
            }
            return isSupportedFormat;
        }

        var validateFileSize = (size, fileSize) => {
            var isSupportedSize = false;
            let fileSizeMb = ((fileSize) / 1048576)
            if (fileSizeMb <= size) {
                isSupportedSize = true
            }
            return isSupportedSize;
        }

        this.uploadFile = (file, type, size, baseUrl, callback) => {
            var isSupportedFormat = validateFileFormat(type, file.name)
            var isSupportedSize = validateFileSize(size, file.size)
            if (!isSupportedSize && !isSupportedFormat) {
                toastr.error("Please upload file size less than " + size + " mb and " + type + " format (supported formats are " + fileFormats[type] + ")")
                callback({
                    isSuccess: false
                })
            }
            if (!isSupportedSize && isSupportedFormat) {
                toastr.error("Please upload file size less than " + size + " mb")
                callback({
                    isSuccess: false
                })
            }
            if (isSupportedSize && !isSupportedFormat) {
                toastr.error("Please upload " + type + " format (supported formats are " + fileFormats[type] + ")")
                callback({
                    isSuccess: false
                })
            }
            if (isSupportedFormat && isSupportedSize) {
                let serverUrl = CONSTANTS.FILE_UPLOAD_URL + APIURLCONST.API_FILE_SERVER_UPLOAD
                this.sucVal = 0;
                Upload.upload({
                    url: serverUrl,
                    data: {
                        file: file,
                        "userPath": baseUrl
                    }
                }).then(function (resp) {
                    console.log(resp)
                    callback({
                        isSuccess: true,
                        url: resp.data
                    });
                }, function (resp) {
                    toastr.error("Please, try after sometimes")
                    console.log(resp);
                    callback({
                        isSuccess: false,
                        errors: resp
                    })
                });
            }
        }

        this.deleteFile = (fileUrl, callback) => {
            try {
                var url = CONSTANTS.FILE_UPLOAD_URL + APIURLCONST.API_FILE_SERVER_DELETE;
                console.log(url)
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function () {
                    if (this.readyState == 4 && this.status == 200) {
                        try {
                            callback({
                                isSuccess: true,
                                data: JSON.parse(xhttp.responseText)
                            });
                        } catch (errors) {
                            callback({
                                isSuccess: false,
                                error: DATACONST.ERRORS.error
                            });
                        }
                    } else {
                        if (this.readyState == 4) {
                            console.log(JSON.parse(xhttp.responseText))
                            callback({
                                isSuccess: false,
                                error: DATACONST.ERRORS.error
                            });
                        }
                    }
                };
                xhttp.open("POST", url, true);
                xhttp.setRequestHeader("Content-type", "application/json");
                xhttp.setRequestHeader("x-auth-token", storage.getItem("token"));
                xhttp.setRequestHeader("x-auth-userid", storage.getItem("user_id"));
                xhttp.onerror = function (e) {
                    console.log(encodeURI)
                    callback({
                        isSuccess: false,
                        error: DATACONST.ERRORS.error
                    });
                };
                xhttp.send(JSON.stringify({ filepath: fileUrl }));
            } catch (ex) {
                console.log(ex);
            }
        }
        this.downloadFile = (fileUrl, callback) => {
            $http.get(CONSTANTS.FILE_UPLOAD_URL + fileUrl, { responseType: 'arraybuffer' }).then(function (response) {
                callback(response);
            });
        }
    })
    .service("storage", function () {
        this.getItem = function (item) {
            let val = JSON.parse(localStorage.getItem(item))
            if (val) {
                if (val.type == "string") {
                    return val.value
                } else {
                    return JSON.parse(val.value)
                }
            }
        }
        this.setItem = function (item, value) {
            let val;
            if (typeof value != "object") {
                val = {
                    type: "string",
                    value: value
                }
            } else {
                val = {
                    type: "object",
                    value: JSON.stringify(value)
                }
            }
            localStorage.setItem(item, JSON.stringify(val))
        }
        this.removeItem = function (item) {
            localStorage.removeItem(item)
        }
        this.clearAll = function () {
            localStorage.clear();
        }
    })
    /******************* SERVICES - END **********************/

    /******************* DIRECTIVES - START **********************/
    .directive('txtMaxlength', ['$compile', '$log', function ($compile, $log) {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, elem, attrs, ctrl) {
                attrs.$set("ngTrim", "false");
                var maxlength = parseInt(attrs.txtMaxlength, 10);
                ctrl.$parsers.push(function (value) {
                    //$log.info("In parser function value = [" + value + "].");
                    if (value.length > maxlength) {
                        //$log.info("The value [" + value + "] is too long!");
                        value = value.substr(0, maxlength);
                        ctrl.$setViewValue(value);
                        ctrl.$render();
                        //$log.info("The value is now truncated as [" + value + "].");
                    }
                    return value;
                });
            }
        }
    }])
    .directive('allowPattern', function allowPatternDirective() {
        return {
            restrict: "A",
            compile: function (tElement, tAttrs) {
                return function (scope, element, attrs) {
                    // I handle key events
                    element.bind("keypress", function (event) {
                        var keyCode = event.which || event.keyCode; // I safely get the keyCode pressed from the event.
                        var keyCodeChar = String.fromCharCode(keyCode); // I determine the char from the keyCode.
                        // If the keyCode char does not match the allowed Regex Pattern, then don't allow the input into the field.
                        if (!keyCodeChar.match(new RegExp(attrs.allowPattern, "i"))) {
                            event.preventDefault();
                            return false;
                        }

                    });
                };
            }
        }
    })
    /******************* DIRECTIVES - END **********************/

    /******************* FILTERS - START **********************/
    .factory("fileReader", ["$q", "$log", function ($q, $log) {
        var onLoad = function (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.resolve(reader.result);
                });
            };
        };

        var onError = function (reader, deferred, scope) {
            return function () {
                scope.$apply(function () {
                    deferred.reject(reader.result);
                });
            };
        };

        var onProgress = function (reader, scope) {
            return function (event) {
                scope.$broadcast("fileProgress", {
                    total: event.total,
                    loaded: event.loaded
                });
            };
        };

        var getReader = function (deferred, scope) {
            var reader = new FileReader();
            reader.onload = onLoad(reader, deferred, scope);
            reader.onerror = onError(reader, deferred, scope);
            reader.onprogress = onProgress(reader, scope);
            return reader;
        };

        var readAsDataURL = function (file, scope) {
            var deferred = $q.defer();

            var reader = getReader(deferred, scope);
            reader.readAsDataURL(file);

            return deferred.promise;
        };

        return {
            readAsDataUrl: readAsDataURL
        };
    }])
    //filter use for trust as resource url
    .filter('trustAsResourceUrl', ['$sce', function ($sce) {
        return function (val) {
            return $sce.trustAsResourceUrl(val);
        };
    }])
    .filter('trustAsHtml', ['$sce', function ($sce) {
        return function (val) {
            return $sce.trustAsHtml(val);
        };
    }]);
/******************* FILTERS - END **********************/


export default coreModule;