// for loading styles we need to load main scss file
import styles from './styles/styles.scss';

// // loading shared module
import './services/core.module';
import './app.constant'
import './app.apiurl'
import './app.dataconstant'
import './app.route'
import './services/core.module'
// loading all module components
import './app.components';

const appModule = angular
	.module('app', [
		// 3rd party modules
		'oc.lazyLoad',
		'ui.router',
		'ngAnimate',
		'ngAria',
		'ngMaterial',
		'ngSanitize',
		'app.core',
		// application specific modules
		'app.login',
		'app.bugHeader',
		'app.sidenav',
		'app.routes',
		'app.constant',
		'app.apiurl',
		'app.dataconstant'
	])

export default appModule;