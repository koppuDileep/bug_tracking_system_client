/**
 * Load all 3rd party imports here so it'll be
 * directly included in vendor.bundle.js file.
 */
import 'angular'
import 'jquery';
import 'oclazyload';
import 'angular-ui-router';
import 'toastr'
import 'angular-animate';
import 'angular-aria';
import 'angular-material';