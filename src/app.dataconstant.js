import uiRouter from 'angular-ui-router';


var module = angular.module('app.dataconstant', [
    uiRouter
    //import modules
]);

module.constant("DATACONST", {
    ROLE_ADMIN: "admin",
    SIDENAV_FEATURES: [{
        "f_id": "users",
        "f_name": "Users ",
        "f_icon": "/assets/images/sidenav/users-solid.png",
        "active": false,
        "state": "viewUser"
    }]
})
