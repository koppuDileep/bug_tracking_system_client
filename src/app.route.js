import uiRouter from 'angular-ui-router';
var module = angular.module('app.routes', [
    uiRouter,
    'oc.lazyLoad'
]);


module.config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    'ngInject';
    $urlRouterProvider.otherwise('/login')
    $stateProvider.state('dashboard', {
        name: 'dashboard',
        url: '/dashboard',
        views: {
            '@': {
                templateProvider: ['$q', function ($q) {
                    let deferred = $q.defer();
                    require.ensure(['./components/dashboard/dashboard.html'], function () {
                        let template = require('./components/dashboard/dashboard.html');
                        deferred.resolve(template);
                    });
                    return deferred.promise;
                }],
                controller: 'dashboardCtrl',
                controllerAs: 'dashboardCtrl'
            }
        },
        resolve: {
            foo: ['$q', '$ocLazyLoad', function ($q, $ocLazyLoad) {
                let deferred = $q.defer();
                require.ensure([], function () {
                    let module = require('./components/dashboard/index.js');
                    $ocLazyLoad.load({
                        name: 'app.dashboard'
                    });
                    deferred.resolve(module);
                });
                return deferred.promise;
            }]
        }
    })
});
export default module