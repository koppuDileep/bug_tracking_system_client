import uiRouter from 'angular-ui-router';


var module = angular.module('app.apiurl', [
    uiRouter
    //import modules
]);

module.constant("APIURLCONST", {
    API_FILE_SERVER_UPLOAD: "/server-uploads",
    API_FILE_SERVER_DELETE: "/delete-file",
    API_LOGIN: "/v1/login",
    API_LOGOUT: "/v1/logout",
});
